new Vue({
	el: '.boondatalist',
	data () {
		return {
			info: null
		}
	},
	mounted () {
		const version = "7"
		console.log("boondatalist: version="+version)
		const id = this.$el.id
		console.log("boondatalist: Id="+id)
		// get the pdb block configuration data
		const apiurl = drupalSettings.pdb.configuration[id].apiurl
		const dataurl = drupalSettings.pdb.configuration[id].dataurl
		const source = drupalSettings.pdb.configuration[id].source_directory
		const product = drupalSettings.pdb.configuration[id].product
		const interval = drupalSettings.pdb.configuration[id].interval
		const formats = drupalSettings.pdb.configuration[id].formats
		const ordering = drupalSettings.pdb.configuration[id].ordering
		const elementId = "datalist"+"_"+source+"_"+product+"_"+interval
		console.log("boondatalist: elementId="+elementId)
		const fieldNameElement = document.getElementById(elementId)
		if( fieldNameElement == null )
		{
			console.log("boondatalist: no element with id ${elementId}")
			return
		}
		var formatlist = ""
		var separator = ""
		for( var formattype in formats )
		{
			if( formats[formattype] == 0 ) { continue }
			formatlist += separator+formattype
			separator = ","
		}
		console.log("boondatalist: debug: formatlist="+formatlist)
		console.log("boondatalist: apiurl="+apiurl)
		console.log("boondatalist: dataurl="+dataurl)
		console.log("boondatalist: source="+source)
		console.log("boondatalist: product="+product)
		console.log("boondatalist: interval="+interval)
		console.log("boondatalist: formats="+formatlist)
		console.log("boondatalist: ordering="+ordering)
		axios.defaults.timeout = 5000 ;	// 5s
		axios
			.get(apiurl,{
				params: {
					source: source,
					product: product,
					interval: interval,
					formats: formatlist,
					//ordering: ordering
				}
			})
			.then(response => {
				//this.info = response.data
				console.log("boondatalist: ajax response:")
				console.log(response)
				// get the pdb block configuration data
				const dataurl = drupalSettings.pdb.configuration[id].dataurl
				const source = drupalSettings.pdb.configuration[id].source_directory
				const product = drupalSettings.pdb.configuration[id].product
				const interval = drupalSettings.pdb.configuration[id].interval
				const formats = drupalSettings.pdb.configuration[id].formats
				const ordering = drupalSettings.pdb.configuration[id].ordering
				const classprefix = drupalSettings.pdb.configuration[id].classprefix
				if( classprefix == "" ) { classprefix = "r" }
				const classtable = classprefix+"Table"
				const classrow = classprefix+"TableRow"
				const classhead = classprefix+"TableHead"
				const classheading = classprefix+"TableHeading"
				const classbody = classprefix+"TableBody"
				const classfoot = classprefix+"TableFoot"
				const classcell = classprefix+"TableCell"
				// sort the response.data array on .year
				if( ordering.localeCompare("ascending") == 0 )		// FIXME replace with == ?
				{
					response.data.sort(function(a, b) {
						var value1 = a.year
						var value2 = b.year
						if (value1 < value2) {
							return -1;
						}
						if (value1 > value2) {
							return 1;
						}
						// must be equal
						return 0;
					});
				}
				if( ordering.localeCompare("descending") == 0 )		// FIXME replace with == ?
				{
					response.data.sort(function(a, b) {
						var value1 = a.year
						var value2 = b.year
						if (value1 > value2) {
							return -1;
						}
						if (value1 < value2) {
							return 1;
						}
						// must be equal
						return 0;
					});
				}
				const elementId = "datalist"+"_"+source+"_"+product+"_"+interval
				const fieldNameElement = document.getElementById(elementId)
				//const dataurl = "https://boonproducts.ucdavis.edu/data"		// FIXME, configurable
				const prefix = dataurl+"/"+source+"/"+product
				var tablehtml = "" ;
				tablehtml += "<div class=\""+classtable+"\">" ;
				tablehtml += "<div class=\""+classbody+"\">" ;
				tablehtml += "<div class=\""+classrow+"\">" ;
				//tablehtml += "<div class=\""+classhead+"\">Year</div>" ;
				for( var formattype in formats )	// each formattype is a key IN formats
				{
					if( formats[formattype] == 0 ) { continue }
					if( formattype.localeCompare("csv") == 0 )	// write CSV header		// FIXME replace with == ?
					{
						tablehtml += "<div class=\""+classhead+"\">CSV File</div>"
					}
					if( formattype.localeCompare("png") == 0 )	// write PNG header		// FIXME replace with == ?
					{
						tablehtml += "<div class=\""+classhead+"\">Preview Image</div>"
					}
				}
				tablehtml += "</div>" // end row
				for( var entry of response.data )	// each entry is an object from the data array
				{ //{"year":"2020","csv":"bml_air_temperature_2020_hourly.csv","png":"bml_air_temperature_2020_hourly.png"}
					//console.log(entry)
					const year = entry.year
					tablehtml += "<div class=\""+classrow+"\">" ;
					//tablehtml += "<div class=\""+classcell+"\">"+year+"</div>"
					for( var formattype in formats )	// formattype is a key in formats
					{
						if( formats[formattype] == 0 ) { continue }
						const file = entry[formattype]
						const path = prefix+"/"+file
						if( formattype.localeCompare("csv") == 0 )	// make an anchor tag		// FIXME replace with == ?
						{
							tablehtml += "<div class=\""+classcell+"\"><a href=\""+path+"\">"+file+"</div>"
						}
						if( formattype.localeCompare("png") == 0 )	// make an img tag		// FIXME replace with == ?
						{
							tablehtml += "<div class=\""+classcell+"\"><a href=\""+path+"\"><img src=\""+path+"\"/></a></div>"
						}
						//console.log("boondatalist: year="+year+" format="+formattype+" file="+file)
					}
					tablehtml += "</div>"	// end row
				}
				tablehtml += "</div>"	// end body
				tablehtml += "</div>"	// end table
				fieldNameElement.innerHTML = tablehtml
				//console.log("boondatalist: tablehtml="+tablehtml)
			})
			.catch(error => {
				console.log(error)
			})
	}
});
