new Vue({
	el: '.boondyn',
	data () {
		return {
			info: null
		}
	},
	mounted () {
		const version = "4"
		console.log("boondyn: version="+version)
		const id = this.$el.id
		const source_directory = drupalSettings.pdb.configuration[id].source_directory
		const namestring = drupalSettings.pdb.configuration[id].names
		var newstring = namestring.replace(/\s+/g,',')
		const apiurl = drupalSettings.pdb.configuration[id].apiurl
		//const url="https://boonproducts.ucdavis.edu/cgi-bin/dyn.pl"
		console.log("boondyn: apiurl="+apiurl)
		console.log("boondyn: source_directory="+source_directory)
		console.log("boondyn: names="+newstring)
		axios
			.get(apiurl,{
				params: {
					source: source_directory,
					names: newstring
				}
			})
			.then(response => {
				//this.info = response.data
				console.log("boondyn: ajax response: ")
				console.log(response)
				for( var [ name, value ] of Object.entries(response.data) )
				{
					const fieldNameElement = document.getElementById(name)
					if( (fieldNameElement !== null) && (value !== "") )
					{
						fieldNameElement.textContent = value
						console.log(`boondyn: set ${name} to ${value}`)
					}
					else
					{
						console.log(`boondyn: name=${name} not set`)
					}
				}
			})
			.catch(error => console.log(error))
	}
});
